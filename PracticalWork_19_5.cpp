// PracticalWork_19_5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>

class Animal
{
private:
   
public:
    std::string Sound;
    Animal() {}
    Animal(std::string NewSound) : Sound(NewSound)
    {}
    void Voice()
    {
        std::cout << "Animal says: " << Sound << "\n";
    }
};

class Dog : public Animal
{
private:
    std::string Sound = "Whoof!";
public:
    
    Dog() 
    {
        Animal::Sound = Sound;
    }

};

class Cat : public Animal
{
private:
    std::string Sound = "Meoow!";
public:

    Cat()
    {
        Animal::Sound = Sound;
    }

};

class Bird : public Animal
{
private:
    std::string Sound = "Tweet!";
public:

    Bird()
    {
        Animal::Sound = Sound;
    }

};

int main()
{
    Animal* arr[25]{};
    for (int i = 0; i < 24; i=i+3)
    {
        arr[i] = new Dog();
        arr[i + 1] = new Cat();
        arr[i + 2] = new Bird();
    }

    for (int i = 0; i < 24; i++)
    {
        arr[i]->Voice();
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
